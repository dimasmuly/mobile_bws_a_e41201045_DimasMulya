import 'package:flutter/material.dart';
import 'package:flutter_messenger/model/video_model.dart';
import 'package:flutter_messenger/ui/video_card.dart';

class HomeScreen extends StatelessWidget {
  final List<VideoModel> videos;
  const HomeScreen({Key? key, required this.videos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      primary: true,
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Explore",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 12),
            height: 40,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Fashion"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Otomotif"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Gaming"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Technology"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Movie"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 8),
                  child: FilterChip(
                    onSelected: (value) {},
                    elevation: 0,
                    label: const Text("Space"),
                  ),
                ),
              ],
            ),
          ),
          const Text(
            "Recommended",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(
            height: 16.10,
          ),
          ListView(
            primary: false,
            shrinkWrap: true,
            children: [
              VideoCard(video: videos[0]),
              const SizedBox(height: 16,),
              VideoCard(video: videos[0]),
              const SizedBox(height: 16,),
              VideoCard(video: videos[0]),
              const SizedBox(height: 16,),
              VideoCard(video: videos[0]),
            ],
            scrollDirection: Axis.vertical,
          )
        ],
      ),
    );
  }
}
