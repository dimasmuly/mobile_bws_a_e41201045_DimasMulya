import 'package:flutter/material.dart';
import 'package:flutter_messenger/model/video_model.dart';
import 'package:flutter_messenger/screens/ExploreScreen.dart';
import 'package:flutter_messenger/screens/HomeScreen.dart';
import 'package:flutter_messenger/ui/video_card.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _pageIndex = 0;

  final _pages = [
    HomeScreen(videos: [
      VideoModel(
        thumbnail: "https://i.ytimg.com/vi/6AI-gFM8gco/maxresdefault.jpg",
        videoTitle: "How to Learn Sing a Song",
        videoDuration: "14:13",
        avatar: "avatar",
        views: "1.2M views",
        date: "24-02-2020",
        channel: "Dimas Adiyaksa",
      )
    ]),
    const ExploreScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //let's start by setting the background of the app
      backgroundColor: const Color(0xFFFAFAFA),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.black),
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.menu),
        ),
        title: Image.asset(
          "assets/Logo_yt.png",
          width: 100,
        ),
        centerTitle: true,
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.cast)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.notifications)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
        ],
      ),
      body: _pages[_pageIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) {
          setState(() {
            _pageIndex = index;
          });
        },
        currentIndex: _pageIndex,
        unselectedItemColor: Colors.black,
        selectedItemColor: Colors.red,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.video_collection),
            label: "Shorts",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: "New Video",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.playlist_play),
            label: "Playlist",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: "settings",
          ),
        ],
      ),
    );
  }
}
