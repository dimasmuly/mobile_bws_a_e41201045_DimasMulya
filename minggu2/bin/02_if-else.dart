import 'dart:io';


void main(List<String> args) {
  print("Selamat datang di permainan werewolf");
  stdout.write("Masukan Nickname : ");
  String nickname = stdin.readLineSync()!;

  if(nickname == ""){
    print("Tolong cepat isi nickname!!!");
  } else {
    var role1 = "Penyihir", role2 = "Werewolf", role3 = "Pangeran", role4 = "Pemburu";

    stdout.write("Pilih Role yang anda mau : ");
    String rolePlayer = stdin.readLineSync()!;

    if(rolePlayer == ''){
      print("$nickname Pilih role terlebih dahulu");
    } else if(rolePlayer == role1){
      print("$nickname $role1 kamu dapat melihat siapa yang menjadi Werewolf ");
    } else if(rolePlayer == role2){
      print("$nickname $role2 kamu dapat melihat siapa yang menjadi Werewolf");
    } else if(rolePlayer == role3){
      print("$nickname $role3 kamu akan memakan mangsa setiap malam");
    } else if(rolePlayer == role4){
      print("$nickname $role4 kamu akan berburu setiap hari");
    } else {
      print("Anda tidak memilih role yang tersedia");
    }

  }
}