import 'package:flutter/material.dart';
import 'package:flutter_messenger/model/video_model.dart';

class VideoCard extends StatelessWidget {
  final VideoModel video;

  const VideoCard({Key? key, required this.video}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.all(8),
        child: 
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              video.videoTitle,
              style: const TextStyle(color: Colors.white, fontSize: 20),
            ),
          ],
        ),
        constraints: const BoxConstraints.expand(height: 150.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.red,
          image: DecorationImage(
              image: NetworkImage(video.thumbnail), fit: BoxFit.cover),
        ),
      ),
    );
  }
}
