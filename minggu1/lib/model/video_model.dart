class VideoModel {
  String thumbnail;
  String videoTitle;
  String videoDuration;
  String avatar;
  String channel;
  String views;
  String date;

  VideoModel(
      {required this.thumbnail,
      required this.videoTitle,
      required this.videoDuration,
      required this.avatar,
      required this.channel,
      required this.views,
      required this.date});
}
